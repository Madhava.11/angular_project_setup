# Node.js and Angular Setup with Ansible

This repository contains an Ansible playbook to set up Node.js, npm, and Angular CLI on your local machine. It allows you to specify different versions of Node.js, npm, and Angular CLI according to your requirements.

### 1. Prerequisites

- Ansible installed on your local machine.
- **Ubuntu**: This playbook is designed for Ubuntu systems. Ensure you have sudo privileges.

### 2. Installing Ansible

```bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

## Usage

### 1. Clone this repository:

```bash
git clone https://gitlab.com/Madhava.11/anguar_project_setup.git
cd node_angular_setup
```
### 2. Run the playbook using Ansible:

```bash
ansible-playbook install_dependencies.yml
```


